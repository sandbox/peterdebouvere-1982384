# Domain to role

## Description

Add one or more roles to a user, when that user registers with an email address
containing a certain domain.

## Use cases

Consider a Drupal website for a large organisation, ACME Inc
(http://www.acme.com). This website has external users who can rate products
and sign up for the company newsletter. This website also has internal users:
users who work for the company and can view more product availability, manage
their absences and expenses, and more.

When an employee registers using his company email address, he is automatically
assigned the 'employee' role, without an administrator having to authorize it.

## Caveats

Make sure there is some sort of email validation, otherwise this module is an
obvious security risk.

The assignment of the roles is only during the registering of the user, so when
a user later changes his email address, he will keep the roles.

## Configuration

Users with the permission "administer domain to role" can configurate the
settings. Go to admin/config/people/accounts/domain_role to add rules.
