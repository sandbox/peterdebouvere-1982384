<?php

/**
 * @file
 * Administration pages.
 */

/**
 * Admin form to add, edit and delete domains to roles.
 */
function domain_role_admin_form() {
  $form = array();

  $domain_role = variable_get('domain_role', array());
  if (!count($domain_role)) {
    $form['no_rules'] = array(
      '#markup' => '<p>' . t('No domain to role rules have yet been added. Add one below.') . '</p>',
    );
  }
  else {

    $header = array(t('Domain'), t('Search method'), t('Roles'), t('Delete'));
    $rows = array();
    foreach ($domain_role as $id => $options) {
      $rows[] = array(
        $options['domain'],
        $options['method'],
        implode(', ', $options['roles']),
        l(t('delete'), 'admin/config/people/accounts/domain_role/delete/' . $id),
      );
    }
    $table = theme('table', array('header' => $header, 'rows' => $rows));

    $form['rules'] = array(
      '#markup' => $table,
    );
  }

  $form['new'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add new'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['new']['domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain'),
    '#description' => t('Do not include the @. Subdomains are OK. Examples: <em>google.com</em>, <em>finance.company.com</em>'),
    '#required' => TRUE,
  );

  $form['new']['method'] = array(
    '#type' => 'select',
    '#title' => t('Search method'),
    '#options' => array(
      'match' => t('Exact match'),
      'contains' => t('Contains'),
    ),
    '#description' => t('Specify if the domain must match exactly, or partially.
                         For example: If you enter <em>google</em> as a domain, and <em>Contains</em> as search method,
                         <em>me@translate.google.com</em> will match, but also <em>someone@notgoogle.com</em>,
                         So use <em>Contains</em> with care.'),
    '#required' => TRUE,
  );

  // Provide all the roles, except 'anonymous' and 'authenticated'.
  $roles = user_roles(TRUE);
  unset($roles[2]);

  $form['new']['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#description' => t('Select the roles that need to be assigned when the domain matches.'),
    '#options' => $roles,
    '#required' => TRUE,
  );

  $form['new']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function domain_role_admin_form_submit(&$form, &$form_state) {
  $domain_role = variable_get('domain_role', array());

  $roles = array();
  foreach ($form_state['values']['roles'] as $role) {
    if (!empty($role)) {
      $roles[$role] = user_role_load($role)->name;
    }
  }

  $domain_role[$form_state['values']['domain']] = array(
    'domain' => $form_state['values']['domain'],
    'method' => $form_state['values']['method'],
    'roles' => $roles,
  );

  variable_set('domain_role', $domain_role);
  drupal_set_message(t('Domain to role rule was succesfully added.'), 'status');
}

/**
 * Generates a confirm form for the deletion of a domain to role rule.
 *
 * @param array $form
 * @param array $form_state
 * @param string $id
 *   The id of the rule, which is the domain.
 */
function domain_role_delete($form, &$form_state, $id) {
  $form = array();

  $form['id'] = array(
    '#type' => 'value',
    '#value' => $id,
  );

  return confirm_form(
    $form,
    t('Are you sure you want to delete this <em>domain to role</em> rule?'),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/config/people/accounts/domain_role',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Implements hook_form_submit().
 *
 * The delete action is confirmed.
 */
function domain_role_delete_submit(&$form, &$form_state) {
  // Collect the rules.
  $domain_role = variable_get('domain_role', array());

  // Unset the rule that needs to be deleted.
  if (isset($domain_role[$form_state['values']['id']])) {
    unset($domain_role[$form_state['values']['id']]);
  }

  // Set the rules into the variable.
  variable_set('domain_role', $domain_role);
  drupal_set_message(t('Domain to role rule succesfully deleted.'), 'status');

  // Redirect to the domain role admin page.
  $form_state['redirect'] = 'admin/config/people/accounts/domain_role';
}
